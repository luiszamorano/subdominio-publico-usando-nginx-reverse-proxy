# LEEME


## Variables de entorno

- **DOMINIO:** Dominio al que se le instala certificados
- **DESTINO:** Donde redireccionará el reverse proxy (formato ip:puerto)
- **EMAIL:*+ E-mail en caso de alerta

## Automatizar certificados:

Programar una tarea en cronetab para que se actualice a las 5 a.m. del primer día de cada segundo mes.

```console
crontab -e
0 5 1 * * /usr/bin/docker compose -f /var/docker/docker-compose.yml up certbot
```


